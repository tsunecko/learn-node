const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Order = require('./order');

const userSchema = new Schema({
  email: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  card: {
    items: [
      {
        productId: {
          type: Schema.Types.ObjectId,
          ref: 'Product',
          required: true,
        },
        quantity: {
          type: Number,
          required: true,
        },
      },
    ],
  },
});

userSchema.methods.addToCard = function(product) {
  // Get same item from db
  const cardProductIndex = this.card.items.findIndex(item => {
    return item.productId.toString() === product._id.toString();
  });
  let newQty = 1;
  const updatedCardItems = [...this.card.items];

  // Increase item`s quantity
  if (cardProductIndex >= 0) {
    newQty = this.card.items[cardProductIndex].quantity + 1;
    updatedCardItems[cardProductIndex].quantity = newQty;
  }
  // Add new item
  else {
    updatedCardItems.push({
      productId: product._id,
      quantity: newQty,
    });
  }

  const updatedCard = {
    items: updatedCardItems,
  };
  this.card = updatedCard;

  return this.save();
};

userSchema.methods.removeFromCard = function(productId) {
  // Get all undeleted products
  const updatedCardItems = this.card.items.filter(item => {
    return item.productId.toString() !== productId.toString();
  });

  this.card.items = updatedCardItems;

  return this.save();
};

userSchema.methods.addOrder = function() {
  // get card
  return this.populate('card.items.productId')
    .execPopulate()
    .then(user => {
      // insert cart to order
      const order = new Order({
        products: user.card.items.map(item => {
          return {
            quantity: item.quantity,
            product: { ...item.productId._doc }, // get data of product
          };
        }),
        user: {
          email: this.email,
          userId: this._id,
        },
      });

      return order.save();
    })
    .then(() => {
      // clear card
      this.card.items = [];
      return this.save();
    })
    .catch(err => console.log(err));
};

module.exports = mongoose.model('User', userSchema);
