const express = require('express');
const adminController = require('../controllers/adminController');
const isAuth = require('../middlewares/is-auth');
const router = express.Router();

router.get('/add-product', isAuth, adminController.getAddProduct);

router.post('/add-product', isAuth, adminController.postAddProducts);

router.get('/edit-product/:productId', isAuth, adminController.getEditProducts);

router.post('/edit-product', isAuth, adminController.postEditProducts);

router.post('/delete-product', isAuth, adminController.deleteProducts);

router.get('/products', isAuth, adminController.getAdminProducts);

module.exports = router;
