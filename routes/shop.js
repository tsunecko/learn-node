const express = require('express');
const shopController = require('../controllers/shopController');
const isAuth = require('../middlewares/is-auth');
const router = express.Router();

router.get('/products', shopController.getProducts);

router.get('/products/:productId', shopController.getProduct);

router.get('/card', isAuth, shopController.getCard);

router.post('/card', isAuth, shopController.postCard);

router.post('/card-delete-item', isAuth, shopController.deleteCard);

// router.get('/checkout', shopController.getCheckout);

router.get('/orders', isAuth, shopController.getOrders);

router.post('/create-order', isAuth, shopController.postOrders);

module.exports = router;
