const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const mongoose = require('mongoose');
const session = require('express-session');
const MongodbStore = require('connect-mongodb-session')(session);
const csrf = require('csurf');
const flash = require('connect-flash');
const env = require('./utils/env');

const app = express();
const store = new MongodbStore({
  uri: env.MONGODB_URI,
  collection: 'sessions',
});
const csrfProtection = csrf({ cookie: false });

// Global config template engines
app.set('view engine', 'pug');
app.set('views', 'views');

// Load routes
const adminRoutes = require('./routes/admin');
const shopRoutes = require('./routes/shop');
const authRoutes = require('./routes/auth');
const errorController = require('./controllers/errorController');

// Models
const User = require('./models/user');

// Body parser
app.use(bodyParser.urlencoded({ extended: false }));
// Call static files from public path (css, img)
app.use(express.static(path.join(__dirname, 'public')));
// Install session
app.use(
  session({
    secret: 'my secret',
    resave: false,
    saveUninitialized: false,
    store: store,
  }),
);
// Add CSRF protection
app.use(csrfProtection);
app.use((req, res, next) => {
  res.locals.isAuth = req.session.isAuth;
  res.locals.csrfToken = req.csrfToken();
  next();
});
// Register flash messages
app.use(flash());
// Create user
app.use((req, res, next) => {
  if (!req.session.user) {
    return next();
  }

  User.findById(req.session.user._id)
    .then(user => {
      req.user = user;
      next();
    })
    .catch(err => console.log(err));
});
// Admin routes
app.use('/admin', adminRoutes);
// Shop Routes
app.use(shopRoutes);
// Auth routes
app.use(authRoutes);
// Errors
app.use(errorController.get404);

// Connect to database
mongoose.set('useNewUrlParser', true);
mongoose.set('useUnifiedTopology', true);
mongoose
  .connect(env.MONGODB_URI)
  .then(() => app.listen(3000))
  .catch(err => console.log(err));
