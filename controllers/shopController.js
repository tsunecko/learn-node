const Product = require('../models/product');
const Order = require('../models/order');

exports.getProducts = (req, res, next) => {
  Product.find()
    .then(products =>
      res.render('shop/product-list', {
        prods: products,
        pageTitle: 'My shop',
        path: '/products',
      }),
    )
    .catch(err => console.log(err));
};

exports.getProduct = (req, res, next) => {
  const prodId = req.params.productId;

  Product.findById(prodId)
    .then(product =>
      res.render('shop/product-detail', {
        product: product,
        pageTitle: product.title,
        path: '/products',
      }),
    )
    .catch(err => console.log(err));
};

exports.getCard = (req, res, next) => {
  req.user
    .populate('card.items.productId')
    .execPopulate()
    .then(user =>
      res.render('shop/card', {
        pageTitle: 'Your card',
        path: '/card',
        products: user.card.items,
      }),
    )
    .catch(err => console.log(err));
};

exports.postCard = (req, res, next) => {
  const prodId = req.body.productId;

  Product.findById(prodId)
    .then(product => req.user.addToCard(product))
    .then(() => res.redirect('/card'))
    .catch(err => console.log(err));
};

exports.deleteCard = (req, res, next) => {
  const prodId = req.body.productId;

  req.user
    .removeFromCard(prodId)
    .then(() => res.redirect('/card'))
    .catch(err => console.log(err));
};

exports.getCheckout = (req, res, next) => {
  // res.render('shop/checkout', {
  //   pageTitle: 'Checkout',
  //   path: '/checkout'
  // });
};

exports.getOrders = (req, res, next) => {
  Order.find({ 'user.userId': req.user._id })
    .then(orders =>
      res.render('shop/orders', {
        pageTitle: 'Your orders',
        path: '/orders',
        orders: orders,
      }),
    )
    .catch(err => console.log(err));
};

exports.postOrders = (req, res, next) => {
  req.user
    .addOrder()
    .then(() => res.redirect('/orders'))
    .catch(err => console.log(err));
};
