const bcrypt = require('bcryptjs');
const User = require('../models/user');
const nodemailer = require('nodemailer');
const sendgridTransport = require('nodemailer-sendgrid-transport');
const env = require('../utils/env');

// Create transporter
const transporter = nodemailer.createTransport(
  sendgridTransport({ auth: { api_key: env.SENDGRID_KEY } }),
);

exports.getLogin = (req, res, next) => {
  res.render('auth/login', {
    path: '/login',
    pageTitle: 'Login',
    errorMsg: req.flash('error'),
  });
};

exports.postLogin = (req, res, next) => {
  const email = req.body.email;
  const pass = req.body.password;

  User.findOne({ email: email })
    .then(user => {
      if (!user) {
        req.flash('error', 'Invalid email or password');
        return res.redirect('/login');
      }

      bcrypt
        .compare(pass, user.password)
        .then(match => {
          if (match) {
            req.session.isAuth = true;
            req.session.user = user;
            return req.session.save(() => res.redirect('/products'));
          }

          req.flash('error', 'Invalid email or password');
          return res.redirect('/login');
        })
        .catch(err => {
          console.log(err);
          req.flash('error', 'Something went wrong');
          return res.redirect('/login');
        });
    })
    .catch(err => console.log(err));
};

exports.postLogout = (req, res, next) => {
  req.session.destroy(err => {
    console.log(err);
    res.redirect('/products');
  });
};

exports.getSignup = (req, res, next) => {
  res.render('auth/signup', {
    path: '/signup',
    pageTitle: 'Signup',
  });
};

exports.postSignup = (req, res, next) => {
  const email = req.body.email;
  const pass = req.body.password;
  const confirmPass = req.body.confirmPass;

  User.findOne({ email: email })
    .then(async user => {
      if (user) {
        req.flash('error', 'User already exists');
        return res.redirect('/signup');
      }

      return bcrypt
        .hash(pass, 10)
        .then(hash => {
          const newUser = new User({
            email: email,
            password: hash,
            card: { items: [] },
          });

          return newUser.save();
        })
        .then(() => {
          res.redirect('/login');

          return transporter.sendMail({
            to: email,
            from: 'shop@learn-node.com',
            subject: 'Sign up succeeded',
            html: '<h1>You successfully sign up</h1>',
          });
        })
        .catch(err => console.log(err));
    })
    .catch(err => console.log(err));
};
