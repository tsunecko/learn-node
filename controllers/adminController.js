const Product = require('../models/product');

exports.postAddProducts = (req, res, next) => {
  const product = new Product({
    title: req.body.title,
    imageUrl: req.body.imageUrl,
    desc: req.body.desc,
    price: req.body.price,
    userId: req.user,
  });

  product
    .save()
    .then(() => res.redirect('/products'))
    .catch(err => console.log(err));
};

exports.getEditProducts = (req, res, next) => {
  const editMode = req.query.edit;
  if (!editMode) {
    res.redirect('/products');
  }

  // Prepare data
  const prodId = req.params.productId;

  Product.findById(prodId)
    .then(product =>
      res.render('admin/edit-product', {
        pageTitle: 'Edit product',
        path: '/admin/edit-product',
        editing: editMode,
        product: product,
      }),
    )
    .catch(err => console.log(err));
};

exports.postEditProducts = (req, res, next) => {
  const prodId = req.body.productId;

  Product.findById(prodId)
    .then(product => {
      product.title = req.body.title;
      product.imageUrl = req.body.imageUrl;
      product.desc = req.body.desc;
      product.price = req.body.price;

      return product.save();
    })
    .then(() => res.redirect('/admin/products'))
    .catch(err => console.log(err));
};

exports.deleteProducts = (req, res, next) => {
  Product.findByIdAndRemove(req.body.productId)
    .then(() => res.redirect('/admin/products'))
    .catch(err => console.log(err));
};

exports.getAdminProducts = (req, res, next) => {
  Product.find()
    .then(products =>
      res.render('admin/products', {
        prods: products,
        pageTitle: 'Admin products',
        path: '/admin/products',
      }),
    )
    .catch(err => console.log(err));
};

exports.getAddProduct = (req, res, next) => {
  res.render('admin/edit-product', {
    pageTitle: 'Add product',
    path: '/admin/add-product',
    editing: false,
  });
};
